package net.pl3x.bukkit.pl3xicons.api.event.translate;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public abstract class TranslateEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancel = false;

    public TranslateEvent(Player player) {
        super(player);
    }

    /**
     * Get cancelled state of translation process.
     *
     * @return True if the translation is cancelled (this does not cancel the Bukkit Event)
     */
    public boolean isCancelled() {
        return cancel;
    }

    /**
     * Set the cancelled state of the translation process.
     *
     * Set to true to cancel the translation process (this does not cancel the Bukkit Event)
     */
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
