package net.pl3x.bukkit.pl3xicons.api.event;

import net.pl3x.bukkit.pl3xicons.api.ResourcePackStatus;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Called when a player sends a resource pack status packet.
 */
public class ResourcePackStatusEvent extends PlayerEvent {
    private static final HandlerList handlers = new HandlerList();
    private final ResourcePackStatus status;
    private final String hash;

    public ResourcePackStatusEvent(Player player, ResourcePackStatus status, String hash) {
        super(player);
        this.status = status;
        this.hash = hash;
    }

    /**
     * Get the status of the packet.
     *
     * @return ResourcePakStatus of the packet
     */
    public ResourcePackStatus getStatus() {
        return status;
    }

    /**
     * Gets the resource pack hash as provided by the packet
     *
     * @return Hash of the resource pack
     */
    public String getHash() {
        return hash;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
