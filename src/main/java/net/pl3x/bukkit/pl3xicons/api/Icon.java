package net.pl3x.bukkit.pl3xicons.api;

/**
 * Represents an Icon
 */
public class Icon {
    private final String hex;
    private final String code;
    private final char character;

    public Icon(String hex, String code) {
        this.hex = hex.toLowerCase();
        this.code = code.toLowerCase();
        this.character = (char) Integer.parseInt(hex, 16);
    }

    /**
     * Get the character hex code
     *
     * @return The hex code
     */
    public String getHex() {
        return hex;
    }

    /**
     * Get the {icon-code} name as set in icons.yml (this does not include the {} brackets).
     *
     * @return The {icon-code} name
     */
    public String getCode() {
        return code.toLowerCase();
    }

    /**
     * Get the Character to replace
     *
     * @return The Character to replace
     */
    public Character getCharacter() {
        return character;
    }

    /**
     * Get an Icon by either character, tag-code, or hex
     *
     * @param string Either an icon's character, tag-code, or hex
     * @return Matching Icon or null if none found
     */
    public static Icon getIcon(String string) {
        if (string == null || string.isEmpty()) {
            return null;
        }
        Icon icon;
        IconManager manager = IconManager.getManager();
        if (string.length() == 1) {
            icon = manager.getIconByChar(string.charAt(0));
            if (icon != null) {
                return icon;
            }
        }
        icon = manager.getIconByCode(string);
        if (icon != null) {
            return icon;
        }
        return manager.getIconByHex(string);
    }
}
