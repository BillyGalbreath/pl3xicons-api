package net.pl3x.bukkit.pl3xicons.api;

/**
 * Resource Pack Status result from the client
 */
public enum ResourcePackStatus {
    SUCCESSFULLY_LOADED(0),
    DECLINED(1),
    FAILED_DOWNLOAD(2),
    ACCEPTED(3);

    private final int id;

    ResourcePackStatus(int id) {
        this.id = id;
    }

    /**
     * Get status by ID.
     *
     * @param id ID of status
     * @return ResourcePackStatus
     */
    public static ResourcePackStatus byID(int id) {
        for (ResourcePackStatus s : values()) {
            if (s.id == id) {
                return s;
            }
        }
        return DECLINED;
    }

    /**
     * Get status by name.
     *
     * @param name Name of status
     * @return ResourcePackStatus
     */
    public static ResourcePackStatus byName(String name) {
        for (ResourcePackStatus s : values()) {
            if (s.name().equals(name)) {
                return s;
            }
        }
        return DECLINED;
    }
}
