package net.pl3x.bukkit.pl3xicons.api.event.translate;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Called when a plugin requests it
 *
 * @deprecated Use IconManager#translate(String) instead
 */
public class IconTranslateEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final String string;
    private String translated;
    private String untranslated;

    /**
     * @param string String to translate
     * @deprecated Use IconManager#translate(String) instead
     */
    public IconTranslateEvent(String string) {
        this.string = string;
    }

    /**
     * Gets the original string
     *
     * @return Original string
     */
    public String getString() {
        return string;
    }

    /**
     * Gets the translated string (has icon characters)
     *
     * @return Translated string
     */
    public String getTranslatedString() {
        return translated;
    }

    /**
     * Gets the untranslated string (has icon {tag} codes)
     *
     * @return Untranslated string
     */
    public String getUntranslatedString() {
        return untranslated;
    }

    /**
     * Sets the translated string
     *
     * @param string Translated string
     */
    public void setTranslatedString(String string) {
        this.translated = string;
    }

    /**
     * Sets the untranslated string
     *
     * @param string Untranslated string
     */
    public void setUntranslatedString(String string) {
        this.untranslated = string;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
