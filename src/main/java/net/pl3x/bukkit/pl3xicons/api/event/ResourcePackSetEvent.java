package net.pl3x.bukkit.pl3xicons.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Called when a the server requests a player to accept a resource pack.
 */
public class ResourcePackSetEvent extends PlayerEvent {
    private static final HandlerList handlers = new HandlerList();
    private final String url;
    private final String hash;

    public ResourcePackSetEvent(Player player, String url, String hash) {
        super(player);
        this.url = url;
        this.hash = hash;
    }

    /**
     * Get the url where the resource pack is located.
     *
     * @return URL for the resource pack
     */
    public String getURL() {
        return url;
    }

    /**
     * Gets the resource pack hash as provided by the packet
     *
     * @return Hash of the resource pack
     */
    public String getHash() {
        return hash;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
