package net.pl3x.bukkit.pl3xicons.api;

import java.util.List;

/**
 * IconManager handles the bulk of all the work for Icons.
 */
public abstract class IconManager {
    private static IconManager manager;

    /**
     * Set the icon manager.
     * <p>
     * DO NOT USE THIS METHOD! IT IS FOR INTERNAL USE ONLY
     *
     * @param manager Icon manager to set
     * @return Icon manager
     */
    public static IconManager setManager(IconManager manager) {
        if (IconManager.manager == null) {
            IconManager.manager = manager;
        }
        return manager;
    }

    /**
     * Get the icon manager
     *
     * @return Icon manager
     */
    public static IconManager getManager() {
        return manager;
    }

    /**
     * Registers an Icon in the Icon DB.
     *
     * @param icon Icon to add
     */
    public abstract void addIcon(Icon icon);

    /**
     * Get a List of all Icons in the DB.
     *
     * @return List of all Icons
     */
    public abstract List<Icon> getAll();

    /**
     * Clear all icons from the DB.
     */
    public abstract void clearIcons();

    /**
     * Get a List of Icons which codes start with matching string (case insensitive).
     *
     * @param code Starts with
     * @return List of matching Icons
     */
    public abstract List<String> getMatching(String code);

    /**
     * Get an Icon by specific code (case sensitive).
     *
     * @param code Code of icon
     * @return Icon with exact code
     */
    public abstract Icon getIconByCode(String code);

    /**
     * Get an Icon by hex.
     *
     * @param hex Hex code of character
     * @return Icon for hex
     */
    public abstract Icon getIconByHex(String hex);

    /**
     * Get an Icon by its Unicode character
     *
     * @param character Unicode character
     * @return Icon for character
     */
    public abstract Icon getIconByChar(Character character);

    /**
     * Replace all {icon-code} representations with their icon characters.
     * <p>
     * Emoticons will be automatically parsed (for backwards compatibility)
     *
     * @param string String to translate
     * @return Translated string
     */
    public abstract String translate(String string);

    /**
     * Replace all {icon-code} representations with their icon characters.
     *
     * @param string         String to translate
     * @param parseEmoticons True to parse emoticons in this translation
     * @return Translated string
     */
    public abstract String translate(String string, boolean parseEmoticons);

    /**
     * Replace all icon characters with their {icon-code} representations.
     *
     * @param string String to untranslate
     * @return Untranslated string
     */
    public abstract String untranslate(String string);

    /**
     * Reloads icons.yml config file
     */
    public abstract void reloadIconConfig();
}
