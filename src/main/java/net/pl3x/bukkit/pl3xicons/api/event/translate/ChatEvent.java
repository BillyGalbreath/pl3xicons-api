package net.pl3x.bukkit.pl3xicons.api.event.translate;

import org.bukkit.entity.Player;

import java.util.IllegalFormatException;
import java.util.Set;

/**
 * Called when a player sends a chat message.
 * <p>
 * If cancelled, the message and format will not get translated to icons.
 */
public class ChatEvent extends TranslateEvent {
    private String format;
    private String message;
    private Set<Player> recipients;

    public ChatEvent(Player player, String format, String message, Set<Player> recipients) {
        super(player);
        this.format = format;
        this.message = message;
        this.recipients = recipients;
    }

    /**
     * Gets the message that the player is attempting to send. This message
     * will be used with {@link #getFormat()}.
     *
     * @return Message the player is attempting to send
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message that the player will send. This message will be used
     * with {@link #getFormat()}.
     *
     * @param message New message that the player will send
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the format to use to display this chat message.
     * <p>
     * When this event finishes execution, the first format parameter is the
     * {@link Player#getDisplayName()} and the second parameter is {@link
     * #getMessage()}
     *
     * @return {@link String#format(String, Object...)} compatible format string
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format to use to display this chat message.
     * <p>
     * When this event finishes execution, the first format parameter is the
     * {@link Player#getDisplayName()} and the second parameter is {@link #getMessage()}
     *
     * @param format {@link String#format(String, Object...)} compatible format string
     * @throws IllegalFormatException if the underlying API throws the exception
     * @throws NullPointerException   if format is null
     * @see String#format(String, Object...)
     */
    public void setFormat(final String format) throws IllegalFormatException, NullPointerException {
        // Oh for a better way to do this!
        try {
            String.format(format, player, message);
        } catch (RuntimeException ex) {
            ex.fillInStackTrace();
            throw ex;
        }

        this.format = format;
    }

    /**
     * Gets a set of recipients that this chat message will be displayed to.
     *
     * @return All Players who will see this chat message
     */
    public Set<Player> getRecipients() {
        return recipients;
    }
}
