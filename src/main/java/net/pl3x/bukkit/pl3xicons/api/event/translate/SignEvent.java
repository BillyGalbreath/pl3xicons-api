package net.pl3x.bukkit.pl3xicons.api.event.translate;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/**
 * Called when a sign is changed by a player.
 * <p>
 * If cancelled, the sign will not get translated to icons.
 */
public class SignEvent extends TranslateEvent {
    private final Block block;
    private final String[] lines;

    public SignEvent(Player player, Block block, String[] lines) {
        super(player);
        this.lines = lines;
        this.block = block;
    }

    /**
     * Gets the block involved in this event.
     *
     * @return The Block which block is involved in this event
     */
    public final Block getBlock() {
        return block;
    }

    /**
     * Gets all of the lines of text from the sign involved in this event.
     *
     * @return the String array for the sign's lines new text
     */
    public String[] getLines() {
        return lines;
    }

    /**
     * Gets a single line of text from the sign involved in this event.
     *
     * @param index index of the line to get
     * @return the String containing the line of text associated with the
     * provided index
     * @throws IndexOutOfBoundsException thrown when the provided index is {@literal > 3 or < 0}
     */
    public String getLine(int index) throws IndexOutOfBoundsException {
        return lines[index];
    }

    /**
     * Sets a single line for the sign involved in this event
     *
     * @param index index of the line to set
     * @param line  text to set
     * @throws IndexOutOfBoundsException thrown when the provided index is {@literal > 3 or < 0}
     */
    public void setLine(int index, String line) throws IndexOutOfBoundsException {
        lines[index] = line;
    }
}
