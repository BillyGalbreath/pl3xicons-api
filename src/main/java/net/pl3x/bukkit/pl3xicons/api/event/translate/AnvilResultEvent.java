package net.pl3x.bukkit.pl3xicons.api.event.translate;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Called when a player clicks item in anvil result slot.
 * <p>
 * If cancelled, the item's name will not get translated to icons.
 */
public class AnvilResultEvent extends TranslateEvent {
    private final Block anvil;
    private final ItemStack result;

    public AnvilResultEvent(Player player, Block anvil, ItemStack result) {
        super(player);
        this.anvil = anvil;
        this.result = result;
    }

    /**
     * Gets the block of the anvil used.
     *
     * @return The block of the anvil used
     */
    public Block getAnvil() {
        return anvil;
    }

    /**
     * Gets the result item the player clicked on.
     *
     * @return The resulting item
     */
    public ItemStack getResult() {
        return result;
    }
}
